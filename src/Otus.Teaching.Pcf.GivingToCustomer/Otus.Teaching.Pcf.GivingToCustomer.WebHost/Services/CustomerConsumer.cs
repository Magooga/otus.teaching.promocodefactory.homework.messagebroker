﻿using MassTransit;
using MassTransitMsg;
using System.Threading.Tasks;
using System;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;
using System.Text.Json;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class CustomerConsumer : IConsumer<CommonMsgCustomer>
    {
        IService _service;
        public CustomerConsumer(IService service)
        {
            _service = service;
        }
        public async Task Consume(ConsumeContext<CommonMsgCustomer> context)
        {
            Console.WriteLine("Receive message from rabbit:");
            Console.WriteLine(JsonSerializer.Serialize(context.Message));

            try
            {
                await _service.ServiceGivePromoCodesToCustomersWithPreferenceAsync(context.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Service GivePromoCodesToCustomersWithPreference Fail {ex.Message}");
            }

        }
    }
}
