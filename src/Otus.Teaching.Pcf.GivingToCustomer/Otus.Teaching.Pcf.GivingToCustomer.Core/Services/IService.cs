﻿using MassTransitMsg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public interface IService
    {
        public Task ServiceGivePromoCodesToCustomersWithPreferenceAsync(CommonMsgCustomer request);
    }
}
