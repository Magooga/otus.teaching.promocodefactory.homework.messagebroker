﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransitMsg;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class MessageAsyncService : IService 
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;

        public MessageAsyncService(IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository,
            IRepository<PromoCode> promoCodesRepository) 
        {
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _promoCodesRepository = promoCodesRepository;
        }
        public async Task ServiceGivePromoCodesToCustomersWithPreferenceAsync(CommonMsgCustomer request)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(request.PreferenceId);

            if (preference == null)
            {
                throw new Exception("Preference not found");
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = MapFromModel(request, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);
        }

        public PromoCode MapFromModel(CommonMsgCustomer request, Preference preference, IEnumerable<Customer> customers)
        {

            var promocode = new PromoCode();
            promocode.Id = request.PromoCodeId;

            promocode.PartnerId = request.PartnerId;
            promocode.Code = request.PromoCode;
            promocode.ServiceInfo = request.ServiceInfo;

            promocode.BeginDate = DateTime.Parse(request.BeginDate).ToUniversalTime();
            promocode.EndDate = DateTime.Parse(request.EndDate).ToUniversalTime();

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            return promocode;
        }
    }
}
