namespace Otus.Teaching.Pcf.Administration.WebHost.Options
{
    public class RabbitSettings
    {
        public string User { get; set; }

        public string Password { get; set; }

        public string Server { get; set; }

        public string VirtualHost { get; set; }

        public ushort Port { get; set; }
    }
}

