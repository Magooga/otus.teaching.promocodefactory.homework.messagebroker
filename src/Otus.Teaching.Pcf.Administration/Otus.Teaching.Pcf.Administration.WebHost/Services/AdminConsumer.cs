using System;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.WebHost.Models;
using System.Text.Json;
using MassTransitMsg;
using Otus.Teaching.Pcf.Administration.Core.Service;


namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{

    public class AdminConsumer : IConsumer<CommonMsgAdmin>
    {
        IService _service;
        public AdminConsumer(IService service)
        {
            _service = service;
        }
        public async Task Consume(ConsumeContext<CommonMsgAdmin> context)
        {
            Console.WriteLine("Receive message from rabbit:");
            Console.WriteLine(JsonSerializer.Serialize(context.Message));

            try 
            {
                await _service.ServiceUpdateAppliedPromocodesAsync(context.Message.PartenId);
            } catch(Exception ex)
            {
                Console.WriteLine($"Service Update Fail {ex.Message}");
            }
            
        }
    }
}