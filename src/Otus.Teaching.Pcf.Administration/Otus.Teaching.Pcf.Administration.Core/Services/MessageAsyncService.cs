using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.Core.Service
{
    public class MessageAsyncService : IService
    {

        private readonly IRepository<Employee> _employeeRepository;

        public MessageAsyncService(IRepository<Employee> repository)
            => _employeeRepository = repository; 

        public async Task ServiceUpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                throw new Exception("Entity Not found"); 

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
        }

    }
}

