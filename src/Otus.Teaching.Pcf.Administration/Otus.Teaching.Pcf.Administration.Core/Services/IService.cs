using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Service
{
    public interface IService
    {
        public Task ServiceUpdateAppliedPromocodesAsync(Guid id);
    }
}

